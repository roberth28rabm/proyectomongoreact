package facci.pm.realmdb;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;

import io.realm.OrderedCollectionChangeSet;
import io.realm.OrderedRealmCollectionChangeListener;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;

public class MainActivity extends AppCompatActivity {

    Realm uiThreadRealm;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Realm.init(this); // context, usually an Activity or Application

        //Open a Realm
        String realmName = "My Project";
        RealmConfiguration config = new RealmConfiguration.Builder().name(realmName).build();
        uiThreadRealm = Realm.getInstance(config);

        addChangeListenerToRealm(uiThreadRealm);

        FutureTask<String> Task = new FutureTask(new BackgroundQuickStart(), "test");
        ExecutorService executorService = Executors.newFixedThreadPool(2);
        executorService.execute(Task);
    }
    private void addChangeListenerToRealm(Realm realm) {
        // all Tasks in the realm
        RealmResults<Task> Tasks = uiThreadRealm.where(Task.class).findAllAsync();
        Tasks.addChangeListener(new OrderedRealmCollectionChangeListener<RealmResults<Task>>() {
            @Override
            public void onChange(RealmResults<Task> collection, OrderedCollectionChangeSet changeSet) {
                // process deletions in reverse order if maintaining parallel data structures so indices don't change as you iterate
//                OrderedCollectionChangeSet.Range[] deletions = changeSet.getDeletionRanges();
//                for (OrderedCollectionChangeSet.Range range : deletions) {
//                    Log.e("QUICKSTART", "Deleted range: " + range.startIndex + " to " + (range.startIndex + range.length - 1));
//                }

                OrderedCollectionChangeSet.Range[] insertions = changeSet.getInsertionRanges();
                for (OrderedCollectionChangeSet.Range range : insertions) {
                    Log.e("QUICKSTART", "Inserted range: " + range.startIndex + " to " + (range.startIndex + range.length - 1));
                }

                OrderedCollectionChangeSet.Range[] modifications = changeSet.getChangeRanges();
                for (OrderedCollectionChangeSet.Range range : modifications) {
                    Log.e("QUICKSTART", "Updated range: " + range.startIndex + " to " + (range.startIndex + range.length - 1));
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // the ui thread realm uses asynchronous transactions, so we can only safely close the realm
        // when the activity ends and we can safely assume that those transactions have completed
        uiThreadRealm.close();
    }

    public class BackgroundQuickStart implements Runnable {
        @Override
        public void run() {
            String realmName = "My Project";
            RealmConfiguration config = new RealmConfiguration.Builder().name(realmName).build();
            Realm backgroundThreadRealm = Realm.getInstance(config);

            //To create a new Task, instantiate an instance of the Task class and add it to the realm in a write block
            Task Task1 = new Task("Roberth28" ,"593984105214", "roberth28rabmgmail.com");
            Task Task2 = new Task("Chavo11", "593987458748","chavo11mjcagmail.com");
            Task Task3 = new Task("Mercedes24", "593974578169","mechita24mjccgmail.com");

            backgroundThreadRealm.executeTransaction (transactionRealm -> {
                transactionRealm.insert(Task1);
                transactionRealm.insert(Task2);
                transactionRealm.insert(Task3);
            });

//You can retrieve a live collection of all items in the realm
            // all Tasks in the realm
            RealmResults<Task> Tasks = backgroundThreadRealm.where(Task.class).findAll();

            //To MODIFY a task, update its properties in a write transaction block
//            Task otherTask = Tasks.get(0);
//            // all modifications to a realm must happen inside of a write block
//            backgroundThreadRealm.executeTransaction( transactionRealm -> {
//                Task innerOtherTask = transactionRealm.where(Task.class).equalTo("name", otherTask.getName()).findFirst();
//                innerOtherTask.setStatus(TaskStatus.Open);
//            });



            //you can DELETE a task by calling the deleteFromRealm() method in a write transaction block:
            //Task yetAnotherTask = Tasks.get(0);
            //String yetAnotherTaskName = yetAnotherTask.getName();
//            // all modifications to a realm must happen inside of a write block
//            backgroundThreadRealm.executeTransaction( transactionRealm -> {
//                Task innerYetAnotherTask = transactionRealm.where(Task.class).equalTo("_id", yetAnotherTaskName).findFirst();
//                innerYetAnotherTask.deleteFromRealm();
//            });



            for (Task task: Tasks
            ) {
                Log.e("Task", "Name: " + task.getNombre_usuario() + " Status: " + task.getStatus()+ "Celular" + task.getCelular_usuario()+ "Correo:" + task.getCorreo_usuario());
            }


            // you can also filter a collection
//            RealmResults<Task> TasksThatBeginWithN = Tasks.where().beginsWith("name", "N").findAll();
//            RealmResults<Task> openTasks = Tasks.where().equalTo("status", TaskStatus.Open.name()).findAll();



            // because this background thread uses synchronous realm transactions, at this point all
            // transactions have completed and we can safely close the realm
            backgroundThreadRealm.close();
        }
    }
}