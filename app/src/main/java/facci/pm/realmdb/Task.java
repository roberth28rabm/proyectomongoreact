package facci.pm.realmdb;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

public class Task extends RealmObject {

    @Required
    private String nombre_usuario;
    private String celular_usuario;
    private String correo_usuario;
   @Required
    private String status = TaskStatus.Open.name();

    public Task(String _nombre_usuario, String _celular_usuario, String _correo_usuario) { this.nombre_usuario = _nombre_usuario; this.celular_usuario = _celular_usuario; this.correo_usuario = _correo_usuario;}
    public Task() {}

    public void setStatus(TaskStatus status) { this.status = status.name(); }
    public String getStatus() { return this.status; }

    public String getNombre_usuario() { return nombre_usuario; }
    public void setNombre_usuario(String nombre_usuario) { this.nombre_usuario = nombre_usuario; }

    public String getCelular_usuario() { return celular_usuario; }
    public void setCelular_usuario(String celular_usuario) { this.celular_usuario = celular_usuario; }

    public String getCorreo_usuario() { return correo_usuario; }
    public void setCorreo_usuario(String correo_usuario) { this.correo_usuario = correo_usuario; }


}
